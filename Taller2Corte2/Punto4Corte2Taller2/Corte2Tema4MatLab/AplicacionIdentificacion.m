%Aplicaci�n para el modelamiento de un sistema din�mico con redes neuronales
%Realizando la codificaci�n de los datos en simulink

close all
clear all
warning off

%Realizar la simulaci�n del modelo
% Se caracteriza el comportamiento del sistema para 1 valor
%sim('DatosPasoR16')
sim('DatosAleatoriosR16')

%Lectura de datos de simulink
P=PP';
T=TT';

%Valores m�ximos y m�nimos
MinMax = [min(P')' max(P')'];

%Red neuronal
%net=newff(MinMax,[2 1],{'tansig' 'purelin'});
net=newff(MinMax,[3 12 8 1],{'tansig' 'tansig' 'tansig' 'purelin'});

%Entrenamiento de la red neuronal
net.trainParam.epochs = 500;
net = train(net,P,T);

%Resultado de la red neuronal
Y = sim(net,P);

%Presentaci�n del resultado
t = 1:length(Y);
plot(t,T,'r',t,Y,'b')

%Figura del error
e = T - Y;
plot(e)

%Valor del MSE
mse = (1/length(e))*sum(e.^2)
