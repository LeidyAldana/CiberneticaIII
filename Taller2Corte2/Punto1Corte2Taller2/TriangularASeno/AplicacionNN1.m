% Corte 2 Taller 2 Punto 1
%Aplicación red neuronal aproximación de funciones

close all
clear all
warning off

%Realizar la simulación
sim('Datos')

%Cargar datos
load Datos

%Rango de la entrada debido a que es una funcion seno
R = [-1 1];

%Configuración capas y neuronas 

% Ejercicio: 2 capas ocultas, neuronas en las capas ocultas: 2
% La última capa es de salida
S = [2 2 1];

%Red neuronal FF
% Primer capa: sigmoide
% Segunda capa: Lineal
net = newff(R,S,{'tansig','purelin','purelin'})        

%Tiempo
t=ScopeData(:,1)';

%Datos entrada
P = ScopeData(:,2)';
 
%Datos salida
T = ScopeData(:,3)';

%Simulación sin entrenar
Y = sim(net,P);

plot(t,T,t,Y)

%Entrenamiento
net.trainParam.min_grad=0.00001;
net = train(net,P,T)

%Simulación con entrenamiento
Y = sim(net,P);
plot(t,T,t,Y)

%Figura del error
e=T-Y;
figure
plot(t,e)

%Valor del MSE
mse = (1/length(e))*sum(e.^2)