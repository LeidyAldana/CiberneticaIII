%Identificación de imágenes mediante redes neuronales

%Red Feed Forward codificando cinco salidas
% netf = newff(R,[7 10 7]); % se gasta las 100 iteraciones, 5x10^-13
netf = newff(R,[7 14 7]); % 100 iteraciones, 2x10-13

%Simulación sin entrenamiento 
Y = sim(netf,P)

%Entrenamiento
netf.trainParam.epochs = 100;
netf = train(netf,P,T);

%Simulación por separado
Y = sim(netf,M)
Y = sim(netf,X)
Y = sim(netf,C)
Y = sim(netf,A)
Y = sim(netf,F)
Y = sim(netf,K)
Y = sim(netf,S)

%Simulación de todos los datos
Y = sim(netf,P)

%Prueba de la red neuronal FF con una imagen prueba 1
Y = sim(netf,C2)

%Prueba de la red neuronal FF con una imagen prueba 1
Y = sim(netf,C3)
