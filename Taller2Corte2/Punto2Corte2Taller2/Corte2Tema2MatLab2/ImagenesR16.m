% Punto 2 Taller 2
%Imágenes básicas de prueba

close all 
clear all
warning off

%Imágenes de forma matricial

% Imagen 1

m=[
    0 0 1 1 0 0;
    0 0 1 1 0 0;
    1 1 1 1 1 1;
    1 1 1 1 1 1;
    0 0 1 1 0 0;
    0 0 1 1 0 0
    ];
figure
imshow(m,'InitialMagnification','fit')

% Imagen 2

x=[
    1 0 0 0 0 1;
    0 1 0 0 1 0;
    0 0 1 1 0 0;
    0 0 1 1 0 0;
    0 1 0 0 1 0;
    1 0 0 0 0 1
    ];
figure
imshow(x,'InitialMagnification','fit')   

% Imagen 3

c=[
    0 0 0 0 0 0;
    0 1 0 0 1 0;
    0 0 0 0 0 0;
    0 1 0 0 1 0;
    0 1 1 1 1 0;
    0 0 0 0 0 0;
    ];
figure
imshow(c,'InitialMagnification','fit')
 
% Imagen 4

 a=[
     0 0 1 1 0 0;
     0 1 0 0 1 0;
     1 0 0 0 0 1;
     1 1 1 1 1 1;
     1 0 0 0 0 1;
     1 0 0 0 0 1
    ];
figure
imshow(a,'InitialMagnification','fit')   

% Imagen 5

 f=[
    0 0 0 0 0 0;
    0 1 1 1 1 0;
    0 1 0 0 0 0;
    0 1 1 1 0 0;
    0 1 0 0 0 0;
    0 1 0 0 0 0
    ];
figure
imshow(f,'InitialMagnification','fit')   

% Imagen 6

 k=[
    0 0 0 0 0 0;
    0 1 0 0 1 0;
    0 1 0 1 0 0;
    0 1 1 0 0 0;
    0 1 0 1 0 0;
    0 1 0 0 1 0
    ];
figure
imshow(k,'InitialMagnification','fit')   

% Imagen 7

 s=[
    0 0 0 0 0 0;
    0 1 1 1 1 0;
    0 1 0 0 0 0;
    0 1 1 1 1 0;
    0 0 0 0 1 0;
    0 1 1 1 1 0
    ];
figure
imshow(s,'InitialMagnification','fit')   

 
%Generación de las cadenas de entrenamiento

% Codificacion de imagenes

M= [m(:,1); m(:,2); m(:,3); m(:,4); m(:,5); m(:,6)];
X= [x(:,1); x(:,2); x(:,3); x(:,4); x(:,5); x(:,6)];
C= [c(:,1); c(:,2); c(:,3); c(:,4); c(:,5); c(:,6)];
A= [a(:,1); a(:,2); a(:,3); a(:,4); a(:,5); a(:,6)];
F= [f(:,1); f(:,2); f(:,3); f(:,4); f(:,5); f(:,6)];
K= [k(:,1); k(:,2); k(:,3); k(:,4); k(:,5); k(:,6)];
S= [s(:,1); s(:,2); s(:,3); s(:,4); s(:,5); s(:,6)];

%Vectorizando todas % Cada columna es un ejemplo
P=[M, X, C, A, F, K, S];

%Función de salida
T=[
    1 0 0 0 0 0 0;
    0 1 0 0 0 0 0;
    0 0 1 0 0 0 0;
    0 0 0 1 0 0 0;
    0 0 0 0 1 0 0;
    0 0 0 0 0 1 0;
    0 0 0 0 0 0 1];

%Rangos de entrada, imagen digital 6x6
R=[zeros(36,1) ones(36,1)];

%Imagen de Prueba 1

c2=[
     0 0 1 1 0 0;
     0 1 0 0 1 0;
     0 0 0 0 0 0;
     1 0 1 0 1 1;
     1 0 0 0 0 0;
     1 0 0 0 0 1
    ];
figure
imshow(c2,'InitialMagnification','fit')   
C2= [c2(:,1); c2(:,2); c2(:,3); c2(:,4); c2(:,5); c2(:,6)];


%Imagen de Prueba 2

c3=[
    0 0 0 0 0 0;
    0 1 0 0 1 0;
    0 0 0 0 0 0;
    0 0 0 0 0 0;
    0 0 1 0 0 0;
    0 0 0 0 0 0
    ];
figure
imshow(c3,'InitialMagnification','fit')   
C3= [c3(:,1); c3(:,2); c3(:,3); c3(:,4); c3(:,5); c3(:,6)];

%Presentación de todas la imágenes de entrenamiento
figure
subplot(3,3,1)
imshow(m,'InitialMagnification','fit')
%imshow(c2,'InitialMagnification','fit')   

subplot(3,3,2)
imshow(x,'InitialMagnification','fit')
subplot(3,3,3)
imshow(c,'InitialMagnification','fit')
subplot(3,3,4)
imshow(a,'InitialMagnification','fit') 
subplot(3,3,5)
imshow(f,'InitialMagnification','fit') 
subplot(3,3,6)
imshow(k,'InitialMagnification','fit') 
subplot(3,3,7)
imshow(s,'InitialMagnification','fit') 
%subplot(3,3,8)
%imshow(c2,'InitialMagnification','fit')   


%Presentación de todas la imágenes de prueba
figure
subplot(1,2,1)
imshow(c2,'InitialMagnification','fit')

subplot(1,2,2)
imshow(c3,'InitialMagnification','fit')
