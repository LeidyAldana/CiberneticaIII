%Aplicación empleando ANFIS predicción de series de tiempo

close all
clear all
warning off

%Cargar la serie de tiempo
%load mgdata.dat
generacionDatos2

%mgdata=mgdata(1:1200,2)';
mgdata=mgdata/100;

%Tomando el maximo 
maximo = max(mgdata);

%Tomando los datos
a=mgdata;
time = a(:, 1);
ts = a(:, 2);
plot(time, ts);
xlabel('Tiempo (sem)'); ylabel('x(t)');
title('Cambio de tasas CDT en Colombia');

%Datos de entrenamiento y de comprobación
%Se emplean 500 datos de entrenamiento
%La predicción se realiza mediante 4 muestras de entrada y 1 salida
trn_data = zeros(500, 5);
chk_data = zeros(500, 5);

%Codificación de los datos de entrenamiento
START = 101;
start = START - 4;
trn_data(:, 1) = ts(start:start+500-1);
start = START - 3;
trn_data(:, 2) = ts(start:start+500-1);
start = START - 2;
trn_data(:, 3) = ts(start:start+500-1);
start = START - 1;
trn_data(:, 4) = ts(start:start+500-1);
start = START - 0;
trn_data(:, 5) = ts(start:start+500-1);

%Codificación de los datos de comprobación
START = 601;
start = START - 4;
chk_data(:, 1) = ts(start:start+500-1);
start = START - 3;
chk_data(:, 2) = ts(start:start+500-1);
start = START - 2;
chk_data(:, 3) = ts(start:start+500-1);
start = START - 1;
chk_data(:, 4) = ts(start:start+500-1);
start = START - 0;
chk_data(:, 5) = ts(start:start+500-1);

%Gráfica de los datos de entrenamiento y de comprobación
index = 118:1117+1;
figure
plot(time(index), ts(index));
axis([min(time(index)) max(time(index)) min(ts(index)) max(ts(index))]);
xlabel('Time (sec)'); ylabel('x(t)');
title('Cambio de tasas CDT en Colombia');

%Generación del sistema difuso a emplear
fismat = genfis1(trn_data,[3 3 3 3],char('gbellmf','gbellmf','gbellmf','gbellmf'));

%Presentación de las funciones de pertenencía empleadas
figure
for input_index=1:4
    subplot(2,2,input_index)
    [x,y]=plotmf(fismat,'input',input_index);
    plot(x,y)
    axis([-inf inf 0 1.2]);
    xlabel(['Input ' int2str(input_index)]);
end

%Entrenamiento del sistema difuso mediante anfis
[trn_fismat,trn_error] = anfis(trn_data, fismat,[],[],chk_data)

%Presentación de las funciones de pertenencia entrenadas
figure
for input_index=1:4
    subplot(2,2,input_index)
    [x,y]=plotmf(trn_fismat,'input',input_index);
    plot(x,y)
    axis([-inf inf 0 1.2]);
    xlabel(['Input ' int2str(input_index)]);
end

%Simulación del sistema con todos los datos
X = [trn_data(:,1:4);chk_data(:,1:4)];
Y = [trn_data(:,5);chk_data(:,5)];
Ys = evalfis(X,trn_fismat);

%Presentación de resultados
figure
hold on
plot(Y,'r')
plot(Ys,'b')
hold off
xlabel('Tiempo (seg)'); ylabel('x(t)');
title('Serie de tiempo cambio de tasa en CDT Colombia');
legend('Reales', 'Simulados')

%Figura del error
e = Y - Ys;
figure
plot(e)
xlabel('Tiempo (seg)'); ylabel('e(t)');
title('Error');

%Error cuadrático medio
N = length(e);
MSE = (1/N)*sum(e.^2)
