%Optimización de un controlador PID utilizando AG

close all
clear all
warning('off','all')

%Variables globales del controlador
global kp; 
global ki;
global kd;

%Opciones del GA
optionsGA = gaoptimset('Display','iter','Generations',50,'PopulationSize',20);

%Optimización con GA
[x,fval] = ga(@Desemp,3,optionsGA);

%Variables del controlador
kp = x(1); ki = x(2); kd = x(3);

%Simulación del sistema de control
[t,xs,ye] = sim('ModeloPlantaR16');
ys = xs(:,5);
plot(t,ys);
