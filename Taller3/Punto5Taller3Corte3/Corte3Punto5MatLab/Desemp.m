function e = Desemp(X) 
%Funci�n objetivo

%Variables globales del controlador
global kp; 
global ki;
global kd;

%Variables del controlador
kp = X(1);
ki = X(2);
kd = X(3);

%Simulaci�n del sistema de control
[t,xs,ye] = sim('ModeloPlantaR16');
%plot(t,ye);

%N�mero total de datos
N = length(ye);

%C�lculo del error
e = (1/N)*sum(ye.^2)
