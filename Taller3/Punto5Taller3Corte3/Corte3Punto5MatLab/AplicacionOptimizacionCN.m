%Optimización de un controlador PID utilizando cuasi-Newton

close all
clear all
warning('off','all')

%Variables globales del controlador
global kp; 
global ki;
global kd;

%Opciones del algoritmo 
options = optimset('Display','iter','MaxIter',50);

%Optimización con gradiente
x0 = [0 0 0];
[x,fval] = fminunc(@Desemp,x0,options);

%Variables del controlador
kp = x(1); ki = x(2); kd = x(3);

%Simulación del sistema de control
[t,xs,ye] = sim('ModeloPlantaR16');
ys = xs(:,5);
plot(t,ys);
