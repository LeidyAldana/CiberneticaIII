%Aplicaci�n optimizaci�n de un sistema difuso que convierte una se�al triangular a seno

close all
clear all
warning off

%Condici�n inicial
%x=[0.2 -0.5 0.2 0 0.2 0.5 0.2 -0.5 0.2 0 0.2 0.5];
x=[0.4375 -1.21 0.321 -0.5681 0.3578 -0.0104 0.4106 0.511 0.587 1.365 0.605 -1.32 0.5229 -0.165 0.587 1.243];


%Datos de entrenamiento
Datos;

%Sistema difuso sin optimizar
a=generafis(x)
Sr=evalfis(T,a);
plot(t,T,t,S,t,Sr)

%Optimizaci�n con algoritmos gen�ticos
optionsga = gaoptimset('Display','iter')
x = ga(@desempe,16,optionsga)

%Sistema difuso entrenado con AG
a=generafis(x)
Sr=evalfis(T,a);
plot(t,T,t,S,t,Sr)

%Figura del error
e = S - Sr;
plot(e)

%Valor del MSE
mse = (1/length(e))*sum(e.^2)
