function f = Jobjetivo(x)
%Funci�n para el c�lculo del desempe�o de un sistema difuso 
%dado por los par�metros del vector x

%Creaci�n del sistema difuso
a = sisdifuso(x);

%Datos de entrenamiento
load Datos;

%Evaluaci�n del sistema difuso
yd = evalfis(X,a);

%C�lculo del error
error=Y-yd;

%Error cuadr�tico
f=1/length(error)*sum(error.^2);
