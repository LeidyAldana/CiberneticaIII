%Optimización de un sistema difuso para la identificación de un sistema dinámico

close all
clear all
warning('off','all')

%Generación de los datos de entrenamiento
DatosPlanta;

% Normalización de los datos
X=X/(4.5419*1.0e+05);
Y=Y/4.5419e+05;

%Valores iniciales
difuso=[0.466 0.0916 0.4565 1.21 0.544 0 0.4598 1.35 0.548 0.0459 0.453 1.337 0.8238 -1.05 0.9604 0.484 0.9487 2.26 0.842 3.65 0.842 3.65 0.842 3.65];

%Opciones
options = optimset('Display','iter','MaxIter',24);

%Función de minimización fminunc
[difuso,fval,exitflag,output] = fminunc(@Jobjetivo,difuso,options);

%Sistema difuso optimizado
FIS = sisdifuso(difuso);

%Datos para comprobación
load Datos;

X=X/(4.5419*1.0e+05);
Y=Y/4.5419e+05;

%Evaluación del sistema difuso
yd = evalfis(X,FIS);

%Error obtenido
error=Y-yd;
mse=1/length(error)*sum(error.^2)

%Presentación de resultados
figure
plot(yd)
hold on

plot(Y)
title('Comparación')
legend('Simulados','Reales')
xlabel('Tiempo');ylabel('Salida')

%Figura del error
figure
plot(error)
title('Error')
xlabel('Tiempo');ylabel('Error')
