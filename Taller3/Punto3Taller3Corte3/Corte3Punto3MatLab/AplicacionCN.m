%Entrenamiento de un sistema difuso para la predicción de una serie 
%de tiempo empleando Cuasi Newton

close all
clear all
warning off

%Opciones
options = optimset('Display','iter','MaxIter',4000,'MaxFunEvals',4000);

%Valores iniciales
difuso=[0.519 1.38 0.519 1.38 0.519 1.38 0.519 1.38 0.519 1.38 0.519 1.38 1 1 1 1 1 1 1 1];

%Implementación del proceso de optimización
[difuso,fval,exitflag,output] = fminunc(@fitnes,difuso,options);

%Recreacion del sistema difuso
a_m = sisdifuso(difuso);

%Datos para comprobación
x=ecg1;

[X Y] = vectorizacion(x);

%Evaluación del sistema difuso
yd = evalfis(X,a_m);

%Error obtenido
error=Y-yd;
e=1/length(error)*sum(error.^2)

%Presentación de los resultados
figure
plot(yd)
hold on
plot(Y,'r')
title('Comparación')
legend('Simulados','Reales')
xlabel('Tiempo');ylabel('Señal')

%Figura del error
figure
plot(error)
title('Error')
xlabel('Tiempo');ylabel('Error')
