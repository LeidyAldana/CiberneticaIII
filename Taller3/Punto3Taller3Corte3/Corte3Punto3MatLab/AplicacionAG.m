%Entrenamiento de un sistema difuso para la predicción de una serie 
%de tiempo empleando Algoritmos Genéticos

clear all
close all
warning off

%Opciones del algoritmo genético
options = gaoptimset('PopulationSize',50,'Generations',200,'PopInitRange',...
    [-0.5;2],'EliteCount',2,'CrossoverFraction',0.8,'PopulationType','doubleVector','TimeLimit',600,'Display','iter');

%Implementación del proceso de optimización
[mejor,fval,reason,output,poblacion] = ga(@fitnes,32,options);

%Sistema difuso optimizado
a_g = sisdifuso(mejor);

%Datos para comprobación
x=ecg1;

[X Y] = vectorizacion(x);

%Evaluación del sistema difuso
yd = evalfis(X,a_g);

%Error obtenido
error=Y-yd;
e=1/length(error)*sum(error.^2)

%Presentación de los resultados
figure
plot(yd)
hold on
plot(Y,'r')
title('Comparación')
legend('Simulados','Reales')
xlabel('Tiempo');ylabel('Señal')

%Figura del error
figure
plot(error)
title('Error')
xlabel('Tiempo');ylabel('Error')
