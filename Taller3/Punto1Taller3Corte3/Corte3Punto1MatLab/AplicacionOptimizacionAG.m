%Aplicaci�n de optimizaci�n empleando Algoritmos Gen�ticos AG

close all
clear all
warning off

%Funci�n Objetivo
% ackleyfcn


%Superficie de la funci�n objetivo
%Escala
x = -5:0.1:5; % 1
y = x; % 2
d=y; % 3
e=d; %4
f=e; %5
g=f; %6
h=g; %7
i=h; %8
j=i; %9
k=j;

a=20;
b=0.2;
c=2*pi;



%Optimizaci�n empleando AG
%Punto inicial
%X0=[2 1];
X0 = [0 -3 0 1 2 1 -1 -3 -2 0];

% Cantidad variables
NV=10;

%Opciones del AG
optionsga = gaoptimset('Display','iter','PopulationSize',25,'Generations',200,'PlotFcns',{@gaplotbestf,@gaplotbestindiv,@gaplotexpectation,@gaplotstopping});

%Funci�n que implementa la optimizaci�n con AG
y = ga(@ackley ,NV,optionsga)

