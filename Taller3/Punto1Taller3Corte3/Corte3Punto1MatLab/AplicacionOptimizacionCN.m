%Aplicaci�n de optimizaci�n empleando Cuasi Newton CN

close all
clear all
warning off

%Funci�n Objetivo de 2D
% ackleyfcn

%Superficie de la funci�n objetivo
%Escala
x = -5:0.1:5; % 1
y = x; % 2
d=y; % 3
e=d; %4
f=e; %5
g=f; %6
h=g; %7
i=h; %8
j=i; %9
k=j;

a=20;
b=0.2;
c=2*pi;


%Optimizaci�n empleando CN
%Punto inicial con 10 variables
%X0 = [2 1];
X0 = [0 0 1 0 0 1 0 0 0 0];

%Opciones del algoritmo
options = optimset('Display','iter');

%Funci�n que implementa la optimizaci�n con CN
X = fminunc(@ackley,X0,options)
