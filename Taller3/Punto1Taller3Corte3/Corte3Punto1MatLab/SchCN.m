%Aplicaci�n de optimizaci�n empleando Cuasi Newton CN

close all
clear all
warning off

%Funci�n Objetivo de 2D
% schaffern1fcn

%Superficie de la funci�n objetivo
%Escala
x = -5:0.1:5;
y = x;
a=20;
b=0.2;
c=2*pi;


%Optimizaci�n empleando CN
%Punto inicial con 10 variables
%X0 = [2 1];
X0 = [0 -3 0 1 1 2 1 0 -1 0];

%Opciones del algoritmo
options = optimset('Display','iter');

%Funci�n que implementa la optimizaci�n con CN
X = fminunc(@schaffern1fcn,X0,options)
