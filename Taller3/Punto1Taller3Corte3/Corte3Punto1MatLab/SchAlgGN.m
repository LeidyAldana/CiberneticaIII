
close all
clear all
warning off

%Función Objetivo de 2D
% schaffern1fcn
% ackleyfcn


%Superficie de la función objetivo
%Escala
x = -5:0.1:5;
y = x;

%Optimización empleando AG
%Punto inicial
X0 = [0 -3 0 1 1 2 1 0 -1 0];

% Cantidad variables
NV=10;

%Opciones del AG
optionsga = gaoptimset('Display','iter','PopulationSize',25,'Generations',200,'PlotFcns',{@gaplotbestf,@gaplotbestindiv,@gaplotexpectation,@gaplotstopping});

%Función que implementa la optimización con AG
y = ga(@schaffern1fcn,NV,optionsga)

