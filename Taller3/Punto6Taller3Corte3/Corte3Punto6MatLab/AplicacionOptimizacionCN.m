%Optimización de un controlador ZPK utilizando cuasi-Newton

close all
clear all
warning('off','all')

%Variables globales del controlador
global a; 
global b;
global k;

%Opciones del algoritmo 
options = optimset('Display','iter','MaxIter',50);

%Optimización con gradiente
x0 = [0 0 0];
[x,fval] = fminunc(@Desemp,x0,options);

%Variables del controlador
a = x(1); b = x(2); k = x(3);

%Simulación del sistema de control
[t,xs,ye] = sim('ModeloControlR16');
ys = xs(:,5);
plot(t,ys);
