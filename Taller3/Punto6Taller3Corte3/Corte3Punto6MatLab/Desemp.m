function e = Desemp(X) 
%Funci�n objetivo

%Variables globales del controlador
global a; 
global b;
global k;

%Variables del controlador
a = X(1);
b = X(2);
k = X(3);

%Simulaci�n del sistema de control
[t,xs,ye] = sim('ModeloControlR16');
%plot(t,ye);

%N�mero total de datos
N = length(ye);

%C�lculo del error
e = (1/N)*sum(ye.^2);
