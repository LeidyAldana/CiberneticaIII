%Optimización de un controlador ZPK utilizando AG

close all
clear all
warning('off','all')

%Variables globales del controlador
global a; 
global b;
global k;

%Opciones del GA
optionsGA = gaoptimset('Display','iter','Generations',50,'PopulationSize',10);

%Optimización con GA
[x,fval] = ga(@Desemp,3,optionsGA);

%Variables del controlador
a = x(1); b = x(2); k = x(3);

%Simulación del sistema de control
[t,xs,ye] = sim('ModeloControlR16');
ys = xs(:,5);
plot(t,ys);
