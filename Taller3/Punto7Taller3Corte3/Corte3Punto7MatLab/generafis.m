function a=generafis(x)
%Funci�n para generar un sistema de inferencia difusa tipo Mandani

%Sistema de inferencia difusa
a = newfis('Control');

%Variable de entrada
a = addvar(a,'input','X',[-5.5 5.5]);
a = addmf(a,'input',1,'B','gaussmf',[x(1) x(2)]);
a = addmf(a,'input',1,'M','gaussmf',[x(3) x(4)]);
a = addmf(a,'input',1,'A','gaussmf',[x(5) x(6)]);

%Variable de salida
a = addvar(a,'output','Y',[-12.5 12.5]);
a = addmf(a,'output',1,'B','gaussmf',[x(7) x(8)]);
a = addmf(a,'output',1,'M','gaussmf',[x(9) x(10)]);
a = addmf(a,'output',1,'A','gaussmf',[x(11) x(12)]);

%Reglas de inferencia
ruleList = [
  	1 1 1 1
   	2 2 1 1
    3 3 1 1];

%Adici�n de reglas de inferencia
a = addrule(a,ruleList);
