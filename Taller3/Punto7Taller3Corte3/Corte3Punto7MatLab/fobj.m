function esm=fobj(P)
%Funci�n para calcular el �ndice de desempe�o

%Sistema difuso como variable global
global a

%Controlador difuso
a = generafis(P);

%Simulaci�n de la planta
[t,x,e] = sim('SistemaControlPR16');

%�ndice de desempe�o
esm = 1/length(e)*sum(e.^2);
