%Optimización de un sistema de control difuso proporcional mediante GA

close all
clear all
warning('off','all')

%Sistema difuso como variable global
global a

%Optimización con algoritmos genéticos
optionsga = gaoptimset('Display','iter','Generations', 10,'PopulationSize',10);
X = ga(@fobj,12,optionsga)

%Sistema difuso optimizado
a = generafis(X);
[t,x,e] = sim('SistemaControlPR16');
ys = x(:,2);
plot(t,ys);
