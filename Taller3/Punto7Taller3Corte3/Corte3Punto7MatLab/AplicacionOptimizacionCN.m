%Optimización de un sistema de control difuso proporcional mediante
%cuasi-Newton

close all
clear all
warning('off','all')

%Sistema difuso como variable global
global a

%Parámetros iniciales del sistema de inferencia
X0=[0.2 -0.5 0.2 0 0.2 0.5 0.2 -0.5 0.2 0 0.2 0.5];

%Sistema difuso sin optimizar
a = generafis(X0);

%Simulación del sistema de control sin optimizar
[t,x,e] = sim('SistemaControlPR16');
ys = x(:,2);
plot(t,ys);
pause(5)

%Optimización con gradiente
options = optimset('Display','iter','MaxIter',4);
X = fminunc(@fobj,X0,options)

%Simulación del sistema de control optimizado
a = generafis(X);
[t,x,e] = sim('SistemaControlPR16');
ys = x(:,2);
plot(t,ys);
