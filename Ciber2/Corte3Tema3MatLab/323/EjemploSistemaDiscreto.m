%Ejemplo de la implementacion de un sistema de tiempo discreto

%Variables y condiciones iniciales
u0 = 0;
y0 = 0; y1 = 1; y2 = 0;

%Entrada tipo paso
u = ones(1,50);

%SimulaciŽon
for n = 1:50
    %Desplazamientos de la entrada
   
    %Valor actual de la entrada
    u0 = u(n);
    %Desplazamientos de la salida
   
    y2 = y1;
    y1 = y0;
    %Valor actual de la salida
    y0 = 0.5*y1+0.5*y2+u0;
    y(n) = y0;
end
%Resultado
figure
stem(y)