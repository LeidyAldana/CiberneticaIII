%Ejemplo de la implementacion de un sistema de tiempo discreto

%Variables y condiciones iniciales
u0 = 0; 
y0 = 0; y1 = 1; 
%Entrada tipo paso
%u = ones(1,50);
%SimulaciŽon
for n = 1:50
    %Desplazamientos de la entrada
    % Funcion x[n]
    u(n)=(0.5)^n;
    %Valor actual de la entrada
    u0 = u(n);
    %Desplazamientos de la salida
    y1 = y0;
    %Valor actual de la salida
    y0 = -y1+u0;
    y(n) = y0;
end
%Resultado
figure
s = stem(y);
s.Color = 'red';