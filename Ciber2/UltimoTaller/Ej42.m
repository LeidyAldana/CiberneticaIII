%Transformacion de un sistema de tiempo continuo a discreto

close all
clear all
clc

%Sistema de tiempo continuo
Gs = tf([1 1],[1 1 1])

%Tiempo de muestreo
Ts = 0.1;

%Transformacion a tiempo discreto mediante invariancia al impulso.
Gz = c2d(Gs,Ts,'inv')

%Transformacion a tiempo discreto mediante transformacion bilineal
Gz = c2d(Gs,Ts,'tustin')