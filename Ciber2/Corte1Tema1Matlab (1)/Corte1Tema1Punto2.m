%An�lisis de estabilidad mediante diagramas de Bode

%Sistema
G = tf(10)*tf([1],[1 0])*tf([1],[10 1])

%Diagramas de bode
bode(G)

%MG y MP
margin(G)

%Valor de estabilidad
k = 10^(18.1/20)

