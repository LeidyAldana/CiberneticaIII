%Aplicacion de redes neuronales para la prediccion de una senal ECG
tic 


close all
clear all
warning off

%Primero se cargan los datos de la se�al ECG
%load DatosECG
generacionDatos3

% Normalización
%datos=datos/100;

mgdata=datos;
mgdata(:,1)=mgdata(:,1)/max(mgdata(:,1));
mgdata(:,2)=mgdata(:,2)/max(mgdata(:,2));
datos=mgdata;

ecg_x1=datos(1:size(datos,1),2)';

%Tomando el maximo y el minimo
minino = min(ecg_x1);
maximo = max(ecg_x1);
hold on
%Se presentan los datos
plot(ecg_x1)
hold off

%Pasando la seal a simulink
N = length(ecg_x1);
time = 1:N;
ecgData = [time' ecg_x1'];



%Generando los respectivos retardos de la se�al en simulink
sim('Simulacion')

%Para el entrenamiento se toma 70% de datos
setenta=717;
Xecg = XECG(1:setenta,:)';  % Se transpone
Yecg = YECG(1:setenta,1)';
t=1:setenta;

hold on
plot(t,Yecg)
%plot(t,Yecgoculto)
hold off

%Red neuronal feed forward de dos capas metodo de entrenamiento backpropagation
%net = newff([minino maximo;minino maximo;minino maximo],[5 20 1],{'tansig' 'tansig' 'purelin'},'trainlm');
% Input vectors, target vectors, transfer function of ith layer
net = newff([minino maximo;minino maximo;minino maximo;minino maximo],[2 20 1],{'tansig' 'tansig' 'purelin'},'trainlm');

%Simulaci�n de la red sin entrenar para imagen portadora
y = sim(net,Xecg);
hold on
plot(t,Yecg,t,y)
hold off


% Mejor precisión, menor error
net.trainParam.epochs = 200;
%net.trainParam.goal = 0.000000000000000000000000000000000000000000000000001;
net.trainParam.goal = 0.0000001;

%Entrenamiento de la red
net = train(net,Xecg,Yecg);
     

%Simulaci�n de la red entrenada
y = sim(net,Xecg);
plot(t,Yecg,t,y)


hold on
%Comparaci�n con todos los datos
Xecg2 = XECG';
Yecg2 = YECG';
y2 = sim(net,Xecg2);
t2 = 1:length(y2);
plot(t2,Yecg2,'m',t2,y2,'g')
lgd=legend;
leg2=legend('Reales', 'Simulados')
xlabel('Pixel'); ylabel('RGB normalizado');
title('RGB imagen portadora');
%lgd.Title.String = 'Conf. 3';
hold off
legendTitle(leg2, 'Config. 3' );
hold on
%Figura del error
e = Yecg2-y2;
plot(e,'r')
hold off
%Valor del MSE
mse = (1/length(e))*sum(e.^2)

PSNR = 10*log((255*255)/mse)/log(10)

toc
