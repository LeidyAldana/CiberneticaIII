%
% Imágenes proyecto



%Imágenes de forma matricial --> esteganografia

% Imagen 1

m=imread('image/224526.PNG');  %  Manzana esteganografia
m=imresize(m, [32 32]); % 128x128 error de 98GB
%figure
%imshow(m,'InitialMagnification','fit')

% Imagen 2

x=imread('image/224626.PNG'); % naranja
x=imresize(x, [32 32]);

%figure
%imshow(x,'InitialMagnification','fit')   

% Imagen 3

c=imread('image/225206.PNG'); % foto
c=imresize(c, [32 32]);

%figure
%imshow(c,'InitialMagnification','fit')
 
% Imagen 4

a=imread('image/225786.PNG'); % camara
a=imresize(a, [32 32]);

%figure
%imshow(a,'InitialMagnification','fit')   

%Imágenes de forma matricial --> originales

% Imagen 5

f=imread('image/224001.JPG'); % manzana
f=imresize(f, [32 32]);

%figure
%imshow(f,'InitialMagnification','fit')   


% Imagen 6

k=imread('image/224581.JPG'); % naranja
k=imresize(k, [32 32]);

%figure
%imshow(k,'InitialMagnification','fit') 


% Imagen 7

s=imread('image/225741.JPG'); % camara
s=imresize(s, [32 32]);

%figure
%imshow(s,'InitialMagnification','fit')   

 
%Generación de las cadenas de entrenamiento

% Codificacion de imagenes con esteganografia

M=[];
for i = 1:size(m,2)    
    M = vertcat(M, m(:,i) );   
end

X=[];
for i = 1:size(x,2)    
    X = vertcat(X, x(:,i) );   
end

C=[];
for i = 1:size(c,2)    
    C = vertcat(C, c(:,i) );   
end

A=[];
for i = 1:size(a,2)    
    A = vertcat(A, a(:,i) );   
end

F=[];
for i = 1:size(f,2)    
    F = vertcat(F, f(:,i) );   
end

K=[];
for i = 1:size(k,2)    
    K = vertcat(K, k(:,i) );   
end

S=[];
for i = 1:size(s,2)    
    S = vertcat(S, s(:,i) );   
end



%Vectorizando todas % Cada columna es un ejemplo
P=[M, X, C, A, F, K, S];

T=[
    1 0 0 0 0 0 0;
    0 1 0 0 0 0 0;
    0 0 1 0 0 0 0;
    0 0 0 1 0 0 0;
    0 0 0 0 1 0 0;
    0 0 0 0 0 1 0;
    0 0 0 0 0 0 1];


%Rangos de entrada, imagen digital
R=[zeros(size(M,1),1) ones(size(M,1),1)];
%R=[zeros(16384,1) ones(16384,1)];

%Imagen de Prueba 1 con esteganografia

c2=imread('image/test1.PNG');  %  otra fig esteganografia
c2=imresize(c2, [32 32]);
%figure
%imshow(c2,'InitialMagnification','fit')   


C2=[];
for i = 1:size(c2,2)    
    C2 = vertcat(C2, c2(:,i) );   
end


%Imagen de Prueba 2


c3=imread('image/225741.JPG'); % otra original
c3=imresize(c3, [32 32]);
%figure
%imshow(c3,'InitialMagnification','fit') 


C3=[];
for i = 1:size(c3,2)    
    C3 = vertcat(C3, c3(:,i) );   
end


%Presentación de todas la imágenes de entrenamiento
figure
subplot(3,3,1)
imshow(m,'InitialMagnification','fit')
%imshow(c2,'InitialMagnification','fit')   

subplot(3,3,2)
imshow(x,'InitialMagnification','fit')
subplot(3,3,3)
imshow(c,'InitialMagnification','fit')
subplot(3,3,4)
imshow(a,'InitialMagnification','fit') 
subplot(3,3,5)
imshow(f,'InitialMagnification','fit') 
subplot(3,3,6)
imshow(k,'InitialMagnification','fit') 
subplot(3,3,7)
imshow(s,'InitialMagnification','fit') 
%subplot(3,3,8)
%imshow(c2,'InitialMagnification','fit')   


%Presentación de todas la imágenes de prueba
figure
subplot(1,2,1)
imshow(c2,'InitialMagnification','fit')

subplot(1,2,2)
imshow(c3,'InitialMagnification','fit')
