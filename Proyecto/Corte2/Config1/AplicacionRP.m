%Identificación de imágenes mediante redes neuronales
tic

%ImagenesR16

%Red perceptrón codificando siete salidas
net = newp(R,7);

%Simulación sin entrenar 
Y = sim(net,P);

%Entrenamiento
net.trainParam.epochs = 100;
net = train(net,P,T);

%Simulación de todos los datos
Y = sim(net,P)

%Simulación por separado
Y = sim(net,M)
Y = sim(net,X)
Y = sim(net,C)
Y = sim(net,A)
Y = sim(net,F)
Y = sim(net,K)
Y = sim(net,S)


%Prueba de la red neuronal con una imagen modificada
Y = sim(net,C2)

%Prueba de la red neuronal con una imagen modificada
Y = sim(net,C3)

toc
