%Identificaci�n de im�genes con esteganografia mediante redes neuronales feed forward

%Red Feed Forward codificando cinco salidas
 netf = newff(R,[7 10 7]); % Input values, target vectors
%netf = newff(R,[512 14 7]); % 

%Simulaci�n sin entrenamiento 
Y1 = sim(netf,P)

%Entrenamiento
netf.trainParam.epochs = 100;
netf = train(netf,P,T);

%Simulaci�n por separado
Y = sim(netf,M)
Y = sim(netf,X)
Y = sim(netf,C)
Y = sim(netf,A)
Y = sim(netf,F)
Y = sim(netf,K)
Y = sim(netf,S)

%Simulaci�n de todos los datos
Y = sim(netf,P)

%Prueba de la red neuronal FF con una imagen prueba 1
Y3 = sim(netf,C2)

%Prueba de la red neuronal FF con una imagen prueba 1
Y4 = sim(netf,C3)
