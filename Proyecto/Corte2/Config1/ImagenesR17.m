%
% Imágenes proyecto

close all 
clear all
warning off

%Imágenes de forma matricial --> esteganografia

% Imagen 1

m=imread('image/224526.PNG');  %  Manzana esteganografia
%m=imresize(m, [512 512]); % 128x128 error de 98GB
m = rgb2gray(m);
m=imresize(m, [512 512]);

figure
imshow(m)
%imshow(m,'InitialMagnification','fit')

% Imagen 2

x=imread('image/224626.PNG'); % naranja
x = rgb2gray(x);
x=imresize(x, [512 512]);
figure
imshow(x)
%figure
%imshow(x,'InitialMagnification','fit')   

% Imagen 3

c=imread('image/225206.PNG'); % foto
c= rgb2gray(c);
c=imresize(c, [512 512]);
figure
imshow(c)
%figure
%imshow(c,'InitialMagnification','fit')
 
% Imagen 4

a=imread('image/225786.PNG'); % camara
a= rgb2gray(a);
a=imresize(a, [512 512]);
figure
imshow(a)
%figure
%imshow(a,'InitialMagnification','fit')   

%Imágenes de forma matricial --> originales

% Imagen 5

f=imread('image/224001.JPG'); % manzana
f= rgb2gray(f);
f=imresize(f, [512 512]);
figure
imshow(f)
%figure
%imshow(f,'InitialMagnification','fit')   


% Imagen 6

k=imread('image/224581.JPG'); % naranja
k= rgb2gray(k);
k=imresize(k, [512 512]);
figure
imshow(k)
%figure
%imshow(k,'InitialMagnification','fit') 


% Imagen 7

s=imread('image/225741.JPG'); % camara
s= rgb2gray(s);
s=imresize(s, [512 512]);
figure
imshow(s)
%figure
%imshow(s,'InitialMagnification','fit')   

 
%Generación de las cadenas de entrenamiento

% Codificacion de imagenes con esteganografia

M=[];
for i = 1:size(m,2)    
    M = vertcat(M, m(:,i) );   
end

X=[];
for i = 1:size(x,2)    
    X = vertcat(X, x(:,i) );   
end

C=[];
for i = 1:size(c,2)    
    C = vertcat(C, c(:,i) );   
end

A=[];
for i = 1:size(a,2)    
    A = vertcat(A, a(:,i) );   
end

F=[];
for i = 1:size(f,2)    
    F = vertcat(F, f(:,i) );   
end

K=[];
for i = 1:size(k,2)    
    K = vertcat(K, k(:,i) );   
end

S=[];
for i = 1:size(s,2)    
    S = vertcat(S, s(:,i) );   
end



%Vectorizando todas % Cada columna es un ejemplo
P=[M, X, C, A, F, K, S];

T=[
    1 0 0 0 0 0 0;
    0 1 0 0 0 0 0;
    0 0 1 0 0 0 0;
    0 0 0 1 0 0 0;
    0 0 0 0 1 0 0;
    0 0 0 0 0 1 0;
    0 0 0 0 0 0 1];


%Rangos de entrada, imagen digital
R=[zeros(size(M,1),1) ones(size(M,1),1)];
%R=[zeros(16384,1) ones(16384,1)];

%Imagen de Prueba 1 con esteganografia

c2=imread('image/test1.PNG');  %  otra fig esteganografia
c2= rgb2gray(c2);
c2=imresize(c2, [512 512]);
figure
imshow(c2) 


C2=[];
for i = 1:size(c2,2)    
    C2 = vertcat(C2, c2(:,i) );   
end


%Imagen de Prueba 2


c3=imread('image/225741.JPG'); % otra original
c3= rgb2gray(c3);
s=imresize(c3, [512 512]);
figure
imshow(c3)


C3=[];
for i = 1:size(c3,2)    
    C3 = vertcat(C3, c3(:,i) );   
end
