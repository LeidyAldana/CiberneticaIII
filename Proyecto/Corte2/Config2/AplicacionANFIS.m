%Identificaci�n de im�genes con esteganografia mediante redes neuronales feed forward

trn_data=double(P/max(P(:,1))); chk_data=double(chk/max(P(:,1)));
%trn_data=double(P); chk_data=double(chk);

%Generaci�n del sistema difuso a emplear
%fismat = genfis1(P,[4 4 4 4 4 4]);
fismat = genfis1(trn_data);

%Entrenamiento%�pocas de entrenamiento
epoch_n = 100;
%[trn_fismat,trn_error] = anfis(trn_data,fismat,epoch_n);
%Entrenamiento del sistema difuso mediante anfis
[trn_fismat,trn_error] = anfis(trn_data, fismat,[],[],chk_data)

%Resultado de la simulaci�n
%ys=evalfis(P(:,1:3),out_fis);

%Simulaci�n del sistema con todos los datos
X = [trn_data(:,1:6);chk_data(:,1:6)];
Y = [trn_data(:,7);chk_data(:,7)];
Ys = evalfis(double(X),trn_fismat);

%evalfis([0 1 1 1 1 1],fismat);

%Presentaci�n de resultados
figure
hold on
plot(Y,'m*')
plot(Ys,'c-')
hold off
xlabel('Tiempo (seg)'); ylabel('x(t)');
title('Reconocimiento esteganografía');
leg1=legend('Reales', 'Simulados')
legendTitle(leg1, 'Config. 2' );
%Figura del error
e = Y - Ys;
figure
plot(e)
xlabel('Tiempo (seg)'); ylabel('e(t)');
title('Error');

%Error cuadr�tico medio
N = length(e);
MSE = (1/N)*sum(e.^2)

PSNR = 10*log((255*255)/MSE)/log(10)