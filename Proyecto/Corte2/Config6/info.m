texto='Alls Well That Ends Well ACT I SCENE I. Rousillon. The COUNTs palace. Enter BERTRAM the COUNTESS of Rousillon HELENA and LAFEU all in black COUNTESS In delivering my son from me I bury a second husband. BERTRAM And I in going, madam, weep oer my fathers death anew: but I must attend his majestys command, to whom I am now in ward, evermore in subjection. LAFEU You shall find of the king a husband, madam you sir a father he that so generally is at all times good must of necessity hold his virtue to you whose ';

codes = unicode2native(texto, 'US-ASCII');
hide=[];
for i=1:size(codes,2)
    hide(i,1)=i;
    hide(i,2)=codes(i);
end

size(hide)