%Aplicaci�n empleando ANFIS predicci�n de series de tiempo

close all
clear all
warning off

tic

%Cargar la serie de tiempo
%load mgdata.dat
generacionDatos3
mgdata=datos;

%mgdata=mgdata/100;
mgdata(:,1)=mgdata(:,1)/max(mgdata(:,1));
mgdata(:,2)=mgdata(:,2)/max(mgdata(:,2));

%Tomando el maximo 
maximo = max(mgdata);

%Tomando los datos
a=mgdata;
time = a(:, 1);
ts = a(:, 2);
plot(time, ts);
xlabel('Pixel'); ylabel('RGB');
title('RGB imagen original');

%Datos de entrenamiento y de comprobaci�n
%Se emplean 500 datos de entrenamiento
%La predicci�n se realiza mediante 4 muestras de entrada y 1 salida
trn_data = zeros(500, 5);
chk_data = zeros(500, 5);

%Codificaci�n de los datos de entrenamiento
START = 101;
start = START - 4;
trn_data(:, 1) = ts(start:start+500-1);
start = START - 3;
trn_data(:, 2) = ts(start:start+500-1);
start = START - 2;
trn_data(:, 3) = ts(start:start+500-1);
start = START - 1;
trn_data(:, 4) = ts(start:start+500-1);
start = START - 0;
trn_data(:, 5) = ts(start:start+500-1);

%Codificaci�n de los datos de comprobaci�n
START = 601;
start = START - 4;
chk_data(:, 1) = ts(start:start+500-1);
start = START - 3;
chk_data(:, 2) = ts(start:start+500-1);
start = START - 2;
chk_data(:, 3) = ts(start:start+500-1);
start = START - 1;
chk_data(:, 4) = ts(start:start+500-1);
start = START - 0;
chk_data(:, 5) = ts(start:start+500-1);

%Gr�fica de los datos de entrenamiento y de comprobaci�n
index = 118:1117+1;
figure
plot(time(index), ts(index));
axis([min(time(index)) max(time(index)) min(ts(index)) max(ts(index))]);
xlabel('Pixel'); ylabel('RGB');
title('RGB');

%Generaci�n del sistema difuso a emplear
fismat = genfis1(trn_data,[3 3 3 3],char('gbellmf','gbellmf','gbellmf','gbellmf'));

%Presentaci�n de las funciones de pertenenc�a empleadas
figure
for input_index=1:4
    subplot(2,2,input_index)
    [x,y]=plotmf(fismat,'input',input_index);
    plot(x,y)
    axis([-inf inf 0 1.2]);
    xlabel(['Input ' int2str(input_index)]);
end

%Entrenamiento del sistema difuso mediante anfis
[trn_fismat,trn_error] = anfis(trn_data, fismat,[],[],chk_data)

%Presentaci�n de las funciones de pertenencia entrenadas
figure
for input_index=1:4
    subplot(2,2,input_index)
    [x,y]=plotmf(trn_fismat,'input',input_index);
    plot(x,y)
    axis([-inf inf 0 1.2]);
    xlabel(['Input ' int2str(input_index)]);
end

%Simulaci�n del sistema con todos los datos
X = [trn_data(:,1:4);chk_data(:,1:4)];
Y = [trn_data(:,5);chk_data(:,5)];
Ys = evalfis(X,trn_fismat);

%Presentaci�n de resultados
figure
clear legend
hold on
plot(Y,'r')
plot(Ys,'b')
xlabel('Pixel'); ylabel('RGB normalizado');
title('RGB imagen portadora');
hold off
lgd1=legend;
leg1=legend('Reales', 'Simulados')
%lgd1.Title.String = 'Conf. 4';
legendTitle(leg1, 'Config. 4' );

%Figura del error
hold on
e = Y - Ys;
figure
plot(e)
xlabel('Pixeles'); ylabel('e(t)');
title('Error');
hold off

%Error cuadr�tico medio
N = length(e);
MSE = (1/N)*sum(e.^2)

PSNR = 10*log((255*255)/MSE)/log(10)

toc
