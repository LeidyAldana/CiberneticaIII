function a = sisdifuso(x)
%Generaci�n del sistema difuso a emplear
%x=ecg1();
%Sistema difuso tipo mandani
a=newfis('Sisdifuso config. 4');

%Entrada 1
a=addvar(a,'input','entrada1',[0 1]);
a=addmf(a,'input',1,'oscuro','gaussmf',[x(1) x(2)]);
a=addmf(a,'input',1,'intermedio','gaussmf',[x(3) x(4)]);
a=addmf(a,'input',1,'claro ','gaussmf',[x(5) x(6)]);

%Entada 2
a=addvar(a,'input','entrada2',[0 1]);
a=addmf(a,'input',2,'oscuro','gaussmf',[x(7) x(8)]);
a=addmf(a,'input',2,'intermedio','gaussmf',[x(9) x(10)]);
a=addmf(a,'input',2,'claro','gaussmf',[x(11) x(12)]);

%Entada 3
a=addvar(a,'input','entrada3',[0 1]);
a=addmf(a,'input',3,'oscuro','gaussmf',[x(13) x(14)]);
a=addmf(a,'input',3,'intermedio','gaussmf',[x(15) x(16)]);
a=addmf(a,'input',3,'claro','gaussmf',[x(17) x(18)]);

%Salida con funciones ''constan'' -- adicionar otra variable
a=addvar(a,'output','salida',[0 1]);
a=addmf(a,'output',1,'oscuro','gaussmf',[x(19) x(20)]);
a=addmf(a,'output',1,'intermedio','gaussmf',[x(21) x(22)]);
a=addmf(a,'output',1,'claro','gaussmf',[x(23) x(24)]); 

%plotmf(a,'input',1)

%Reglas
ruleList=[   
    1   1   1   1   1   1;
    1   1   2   2   1   1; 
    1   1   3   3   1   1; 
    1   2   1   1   1   1; 
    1   2   2   2   1   1;
    1   2   3   3   1   1;     
    1   3   1   2   1   1; 
    1   3   2   2   1   1;
    1   3   3   3   1   1;
    2   1   1   1   1   1;
    2   1   2   2   1   1; 
    2   1   3   3   1   1; 
    2   2   1   1   1   1; 
    2   2   2   2   1   1;
    2   2   3   3   1   1;     
    2   3   1   2   1   1; 
    2   3   2   2   1   1;
    2   3   3   3   1   1;
    3   1   1   1   1   1;
    3   1   2   2   1   1; 
    3   1   3   3   1   1; 
    3   2   1   1   1   1; 
    3   2   2   2   1   1;
    3   2   3   3   1   1;     
    3   3   1   2   1   1; 
    3   3   2   2   1   1;
    3   3   3   3   1   1;
         ];

a = addrule(a,ruleList);


% Evaluar el sistema
%Y = evalfis([0 1 1],a)