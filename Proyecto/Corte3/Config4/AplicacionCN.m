%Entrenamiento de un sistema difuso para la predicci�n de una serie 
%de tiempo empleando Cuasi Newton

close all
clear all
warning off

%Opciones
options = optimset('Display','iter','MaxIter',4000,'MaxFunEvals',4000);

%Valores iniciales
difuso=[0.1855 0.0316 0.1904 0.517 0.186 0.967 0.1855 0.0316 0.1904 0.517 0.186 0.967 0.1855 0.0316 0.1904 0.517 0.186 0.967 0.1855 0.0316 0.1904 0.517 0.186 0.967 ];

%Implementaci�n del proceso de optimizaci�n
[difuso,fval,exitflag,output] = fminunc(@fitnes,difuso,options);

%Recreacion del sistema difuso
a_m = sisdifuso(difuso);

%Datos para comprobaci�n
x=ecg1;

[X Y] = vectorizacion(x);

%Evaluaci�n del sistema difuso
yd = evalfis(X,a_m);

%Error obtenido
error=Y-yd;
e=1/length(error)*sum(error.^2)

PSNR = 10*log((255*255)/e)/log(10)

%Presentaci�n de los resultados
figure
plot(yd,'k')
hold on
plot(Y,'m')
title('Comparacion Configuracion 4 - Cuasi Newton')
legend('Simulados','Reales')
xlabel('Pixel');ylabel('R+G+B Normalizado')

%Figura del error
figure
plot(error,'r')
title('Error Configuracion 4 - Cuasi Newton')
xlabel('Pixel');ylabel('R+G+B Normalizado')
