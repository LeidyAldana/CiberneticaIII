function [X Y] = vectorizacion(x)
%Esta funci�n crea la matriz de datos de entrada y salida para el
%entrenamiento para un sistema con 3 retardos tipo FIR

%x=ecg1();

%Tama�o del vector x
m=max(size(x));

%Codificaci�n para el entrenamiento
%[[x(k-3) x(k-2), x(k-1)], x(k)]

% 4 retardos
xk_4=x(1:m-4);
xk_3=x(2:m-3);
xk_2=x(3:m-2);
xk_1=x(4:m-1);
xk=x(5:m);

X=[xk_4 xk_3 xk_2 xk_1]; 
Y=xk;

%size(X) 
%size(Y)