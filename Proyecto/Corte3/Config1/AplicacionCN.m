%Entrenamiento de un sistema difuso para la predicción de una serie 
%de tiempo empleando Cuasi Newton

close all
clear all
warning off

%Opciones
options = optimset('Display','iter','MaxIter',4000,'MaxFunEvals',4000);

%Valores iniciales
difuso=[1.02 0.044 1.033 2.55 1.02 0.044 1.033 2.55 1.02 0.044 1.033 2.55 1.02 0.044 1.033 2.55 0.516 -1.218 0.516 -0.567 0.516 0.2524 0.516 0.9932 0.516 1.768 0.516 2.396 0.516 3.137 0.516 3.653];

%Implementación del proceso de optimización
[difuso,fval,exitflag,output] = fminunc(@fitnes,difuso,options);

%Recreacion del sistema difuso
a_m = sisdifuso(difuso);

%Datos para comprobación
x=ecg1;

[X Y] = vectorizacion(x);

%Evaluación del sistema difuso
yd = evalfis(X,a_m);

%Error obtenido
error=Y-yd;
e=1/length(error)*sum(error.^2)

PSNR = 10*log((255*255)/e)/log(10)

%Presentación de los resultados
figure
plot(yd,'m')
hold on
plot(Y,'c')
title('Comparación Configuración 1 - Cuasi Newton')
legend('Simulados','Reales')
xlabel('Pixel');ylabel('R+G+B Normalizado')

%Figura del error
figure
plot(error,'g')
title('Error Configuración 1 - Cuasi Newton')
xlabel('Pixel');ylabel('R+G+B Normalizado')
