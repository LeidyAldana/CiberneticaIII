%Entrenamiento de un sistema difuso para la predicci�n de una serie 
%de tiempo empleando Algoritmos Gen�ticos

clear all
close all
warning off

%Opciones del algoritmo gen�tico
options = gaoptimset('PopulationSize',50,'Generations',200,'PopInitRange',...
    [-0.5;2],'EliteCount',2,'CrossoverFraction',0.8,'PopulationType','doubleVector','TimeLimit',1800,'Display','iter');

%Implementaci�n del proceso de optimizaci�n
[mejor,fval,reason,output,poblacion] = ga(@fitnes,36,options);

%Sistema difuso optimizado
a_g = sisdifuso(mejor);

%Datos para comprobaci�n
x=ecg1;

[X Y] = vectorizacion(x);

%Evaluaci�n del sistema difuso
yd = evalfis(X,a_g);

%Error obtenido
error=Y-yd;
e=1/length(error)*sum(error.^2)


PSNR = 10*log((255*255)/e)/log(10)

%Presentaci�n de los resultados
figure
plot(yd)
hold on
plot(Y,'y')
title('Comparación Configuración 2 - Algoritmos Geneticos')
legend('Simulados','Reales')
xlabel('Pixel');ylabel('R+G+B Normalizado')

%Figura del error
figure
plot(error,'k')
title('Error Configuración 2 - Algoritmos Geneticos')
xlabel('Pixel');ylabel('R+G+B Normalizado')
