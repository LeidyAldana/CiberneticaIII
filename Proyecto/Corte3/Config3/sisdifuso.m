function a = sisdifuso(x)
%Generaci�n del sistema difuso a emplear
%x=ecg1();
%Sistema difuso tipo Sugeno -- cambiar a mandani
a=newfis('Sisdifuso config.3');
%a.andMethod = 'prod';
%a.orMethod = 'max';
%a.defuzzMethod = 'wtaver';
%a.impMethod = 'prod';
%a.aggMethod = 'sum';

%Entrada 1
a=addvar(a,'input','entrada1',[-1.5 4]);
a=addmf(a,'input',1,'in1mf1','gaussmf',[x(1) x(2)]);
a=addmf(a,'input',1,'in1mf2','gaussmf',[x(3) x(4)]);

%Entada 2
a=addvar(a,'input','entrada2',[-1.5 4]);
a=addmf(a,'input',2,'in2mf1','gaussmf',[x(5) x(6)]);
a=addmf(a,'input',2,'in2mf2','gaussmf',[x(7) x(8)]);


%Salida con funciones ''constan'' -- adicionar otra variable
a=addvar(a,'output','salida',[-1.5 4]);
a=addmf(a,'output',1,'out1mf1','gaussmf',[x(9) x(10)]);
a=addmf(a,'output',1,'out1mf2','gaussmf',[x(11) x(12)]);

%plotmf(a,'input',1)

%Reglas
ruleList=[   
  1   1   1   1   1;
  1   2   2   1   1; 
  2   1   1   1   1; 
  2   2   2   1   1;  
         ];

a = addrule(a,ruleList);


% Evaluar el sistema
%Y = evalfis([0 1 ],a)