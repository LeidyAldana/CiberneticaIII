function [X Y] = vectorizacion(x)
%Esta funci�n crea la matriz de datos de entrada y salida para el
%entrenamiento para un sistema con 3 retardos tipo FIR

%x=ecg1();

%Tama�o del vector x
m=max(size(x));

%Codificaci�n para el entrenamiento
%[[x(k-3) x(k-2), x(k-1)], x(k)]

% 4 retardos
xk_2=x(1:m-2);
xk_1=x(2:m-1);
xk=x(3:m);

X=[xk_2 xk_1]; 
Y=xk;

%size(X) 
%size(Y)