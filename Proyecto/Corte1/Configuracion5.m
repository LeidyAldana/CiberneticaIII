% Config 5 ~ 1
%close all
%clear all
warning('off')

% Sistema
a=newfis('Config5');

% Variable de entrada: diferencia de rojo dR
a=addvar(a,'input','dR',[0 255]);  % Rango en la escala de colores

%Funciones de pertenencia
a=addmf(a,'input',1,'ZE','trimf', [-105.1 0.9256 140.1]);
a=addmf(a,'input',1,'LR','trimf', [118 255 362.1]);
plotmf(a,'input',1)

%Variable de entrada: diferencia de Verde dG
a=addvar(a,'input','dG',[0 255]);

%Funciones de pertenencia
a=addmf(a,'input',2,'ZE','trimf', [-105.1 0.9256 140.1]);
a=addmf(a,'input',2,'LR','trimf', [118 255 362.1]);
plotmf(a,'input',2)


% Variable de entrada: diferencia de azul dB
a=addvar(a,'input','dB',[0 255]);

%Funciones de pertenencia
a=addmf(a,'input',3,'ZE','trimf', [-105.1 0.9256 140.1]);
a=addmf(a,'input',3,'LR','trimf', [118 255 362.1]);
plotmf(a,'input',3)


% Variable de salida: Similitud
a=addvar(a,'output','Similitud',[0 100]);

a=addmf(a,'output',1,'NS','trimf', [-0.4167 0 0.4167]);
a=addmf(a,'output',1,'MS','trimf', [0.08333 0.5 0.9167]);
a=addmf(a,'output',1,'ES','trimf', [0.5833 1 1.417]);
plotmf(a,'output',1)%Funciones de pertenencia


%Reglas de inferencia dR dG dB Sim


ruleList=[
    1 1 1 3 1 1    
    1 1 2 3 1 1
    1 2 1 2 1 1
    1 2 2 1 1 1
    2 1 1 3 1 1
    2 1 2 2 1 1
    2 2 1 1 1 1
    2 2 2 1 1 1
  	];

a = addRule(a,ruleList);

% Sistema difuso
fuzzy(a)