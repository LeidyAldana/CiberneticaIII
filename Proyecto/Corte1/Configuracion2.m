%  Configuracion 2 reglas
%  Proyecto esteganografía

%close all
%clear all
%warning('off')

% Sistema
a=newfis('Config2');

% Variable de entrada: diferencia de rojo dR
a=addvar(a,'input','dR',[0 255]);  % Rango en la escala de colores

%Funciones de pertenencia
a=addmf(a,'input',1,'ZE','gaussmf', [21.6 3.08]);
a=addmf(a,'input',1,'BS','gaussmf',  [22.1 64.78]);
a=addmf(a,'input',1,'MD','gaussmf', [19 130.1]);
a=addmf(a,'input',1,'CL','gaussmf', [18.4 193.1] );
a=addmf(a,'input',1,'LR','gaussmf',  [17.7 251.9]);
plotmf(a,'input',1)

%Variable de entrada: diferencia de Verde dG
a=addvar(a,'input','dG',[0 255]);

%Funciones de pertenencia
a=addmf(a,'input',2,'ZE','gaussmf', [21.6 3.08]);
a=addmf(a,'input',2,'BS','gaussmf',  [22.1 64.78]);
a=addmf(a,'input',2,'MD','gaussmf', [19 130.1]);
a=addmf(a,'input',2,'CL','gaussmf', [18.4 193.1] );
a=addmf(a,'input',2,'LR','gaussmf',  [17.7 251.9]);
plotmf(a,'input',2)


% Variable de entrada: diferencia de azul dB
a=addvar(a,'input','dB',[0 255]);

%Funciones de pertenencia
a=addmf(a,'input',3,'ZE','gaussmf', [21.6 3.08]);
a=addmf(a,'input',3,'BS','gaussmf',  [22.1 64.78]);
a=addmf(a,'input',3,'MD','gaussmf', [19 130.1]);
a=addmf(a,'input',3,'CL','gaussmf', [18.4 193.1] );
a=addmf(a,'input',3,'LR','gaussmf',  [17.7 251.9]);
plotmf(a,'input',3)


% Variable de salida: Similitud
a=addvar(a,'output','Similitud',[0 100]);

a=addmf(a,'output',1,'NS','gaussmf', [11.56 -0.8045]);
a=addmf(a,'output',1,'SS','gaussmf',  [10.63 25.13]);
a=addmf(a,'output',1,'MS','gaussmf',  [10.97 49.98]);
a=addmf(a,'output',1,'QS','gaussmf', [10.6 75.05] );
a=addmf(a,'output',1,'ES','gaussmf', [11.46 99.85] );
plotmf(a,'output',1)%Funciones de pertenencia


%Reglas de inferencia


ruleList=[
  	1 1 1 5 1 1
    1 1 2 5 1 1
   	1 1 3 5 1 1
    1 1 4 4 1 1 
    1 1 5 4 1 1
    1 2 1 5 1 1
    1 2 2 5 1 1
    1 2 3 4 1 1
    1 2 4 4 1 1
    1 2 5 3 1 1
    1 3 1 5 1 1
    1 3 2 4 1 1
    1 3 3 4 1 1
    1 3 4 3 1 1 
    1 3 5 3 1 1
    1 4 1 4 1 1
    1 4 2 4 1 1
    1 4 3 3 1 1
    1 4 4 3 1 1
    1 4 5 2 1 1 
    1 5 1 4 1 1
    1 5 2 3 1 1
    1 5 3 3 1 1
    1 5 4 2 1 1
    1 5 5 2 1 1
    2 1 1 5 1 1
    2 1 2 5 1 1    
    2 1 3 4 1 1
    2 1 4 4 1 1
    2 1 5 3 1 1
    2 2 1 4 1 1
    2 2 2 4 1 1
    2 2 3 3 1 1
    2 2 4 3 1 1
    2 2 5 2 1 1
    2 3 1 3 1 1
    2 3 2 3 1 1
    2 3 3 2 1 1
    2 3 4 2 1 1
    2 3 5 1 1 1
    2 4 1 4 1 1
    2 4 2 4 1 1
    2 4 3 3 1 1
    2 4 4 3 1 1
    2 4 5 2 1 1
    2 5 1 3 1 1
    2 5 2 3 1 1
    2 5 3 2 1 1 
    2 5 4 2 1 1
    2 5 5 1 1 1
    3 1 1 4 1 1
    3 1 2 4 1 1
    3 1 3 3 1 1
    3 1 4 3 1 1
    3 1 5 2 1 1
    3 2 1 3 1 1
    3 2 2 3 1 1
    3 2 3 2 1 1
    3 2 4 2 1 1
    3 2 5 1 1 1
    3 3 1 2 1 1
    3 3 2 2 1 1
    3 3 3 1 1 1
    3 3 4 1 1 1
    3 3 5 1 1 1
    3 4 1 3 1 1 
    3 4 2 3 1 1
    3 4 3 2 1 1 
    3 4 4 2 1 1
    3 4 5 1 1 1
    3 5 1 3 1 1
    3 5 2 3 1 1
    3 5 3 2 1 1 
    3 5 4 2 1 1
    3 5 5 1 1 1
    4 1 1 4 1 1
    4 1 2 4 1 1
    4 1 3 3 1 1
    4 1 4 3 1 1
    4 1 5 2 1 1
    4 2 1 4 1 1
    4 2 2 3 1 1
    4 2 3 3 1 1 
    4 2 4 2 1 1
    4 2 5 2 1 1
    4 3 1 3 1 1 
    4 3 2 3 1 1
    4 3 3 2 1 1
    4 3 4 2 1 1
    4 3 5 1 1 1 
    4 4 1 3 1 1
    4 4 2 3 1 1
    4 4 3 2 1 1
    4 4 4 2 1 1
    4 4 5 1 1 1
    4 5 1 2 1 1
    4 5 2 2 1 1
    4 5 3 2 1 1
    4 5 4 1 1 1
    4 5 5 1 1 1
    5 1 1 3 1 1 
    5 1 2 3 1 1
    5 1 3 2 1 1
    5 1 4 2 1 1
    5 1 5 1 1 1 
    5 2 1 3 1 1 
    5 2 2 3 1 1
    5 2 3 2 1 1
    5 2 4 2 1 1
    5 2 5 1 1 1
    5 3 1 2 1 1
    5 3 2 2 1 1 
    5 3 3 2 1 1 
    5 3 4 1 1 1
    5 3 5 1 1 1
    5 4 1 2 1 1
    5 4 2 2 1 1
    5 4 3 2 1 1 
    5 4 4 1 1 1
    5 4 5 1 1 1
    5 5 1 1 1 1
    5 5 2 1 1 1 
    5 5 3 1 1 1
    5 5 4 1 1 1
    5 5 5 1 1 1];

a = addRule(a,ruleList);

% Sistema difuso
fuzzy(a)

% Evaluar el sistema [dR, dG, dB]
Y = evalfis([0 1 1],a)

