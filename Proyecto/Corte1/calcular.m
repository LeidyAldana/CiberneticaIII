
%% Prueba 1

% Imagen principal 

img1 = imread('Muestras/Rene_Magritte_18.jpg')
size(img1);
figure, imshow(img1);

red1 = img1(:,:,1);           % Matrices de 592 x 807
green1 = img1(:,:,2);
blue1 = img1(:,:,3);

% Imagen a ocultar 

img2 = imread('Muestras/paciente1.png')
size(img2);
figure, imshow(img2);

red2 = img2(:,:,1);           % Matrices de 100 x 80
% convirtiendo al mismo tamaño
red2Conv = imresize(red2, [size(img1,1),size(img1,2)] );
size(red2Conv);

green2 = img2(:,:,2);
% convirtiendo al mismo tamaño
green2Conv = imresize(green2,  [size(img1,1),size(img1,2)]);

blue2 = img2(:,:,3);
% convirtiendo al mismo tamaño
blue2Conv = imresize(blue2,  [size(img1,1),size(img1,2)]);

dR=red1(:,:)-red2Conv(:,:);
dR=double(dR);

dG=green1(:,:)-green2Conv(:,:);
dG=double(dG);
dB=blue1(:,:)-blue2Conv(:,:);
dB=double(dB);


for i=1: size(dR,1)
    for j=1: size(dR,2)
        % Se evalua 
        modelo(i,j) = evalfis(ModeloFuzzyArticulo, [ dR(i,j) dG(i,j) dB(i,j) ]);
        %modelo=1;
    end
end


%% Graficas por capas

a = zeros(size(img, 1), size(img, 2));
just_red = cat(3, red, a, a);
size(just_red)
figure, imshow(just_red)

just_green = cat(3, a, green, a);
figure, imshow(just_green)

just_blue = cat(3, a, a, blue);
figure, imshow(just_blue)



