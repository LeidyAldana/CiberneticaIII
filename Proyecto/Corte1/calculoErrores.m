%% Calculo de los errores para graficacion



% ---> Error cuadratico medio MSE 

% Configuracion 1

imgCifrada1 = imread('Muestras/ImagenConMensajeResultado1.bmp');
imgOriginal1 = imread('Muestras/Vasiliy_Kandinskiy_11.jpg');

errPaciente1=immse(imgCifrada1, imgOriginal1)

errorDatosNum=immse(modelo1, modelo2)

% Configuracion 2

imgCifrada2 = imread('ImagenConMensaje.bmp');
imgOriginal2 = imread('Muestras/Vasiliy_Kandinskiy_11.jpg');

errMSEConfig2=immse(modelo1, modelo2)

errMSEConfig3=immse(modelo1,modelo3)
% Config 4
errMSEConfig4=immse(modelo1, modelo4)

errMSEConfig5=immse(modelo1, modelo5)

errMSEResCual=immse(imread('Config4Resultado.bmp'),imread('ResultadoEsperado.bmp'))
errMSEResCual2=immse(imread('Config4Resultado.bmp'),imread('Muestras/Vasiliy_Kandinskiy_11.jpg'))
% ---> Ruido que afecta a la imagen

% Configuracion 1

errPaciente1=psnr(imgCifrada1, imgOriginal1)

% Config 2
errPSNRConfig2=psnr(modelo1, modelo2)
errPSNRConfig3=psnr(modelo1, modelo3)

% Config 4

errConf2p=psnr(modelo4, modelo1)
errPSNRConfig5=psnr(modelo1, modelo5)

errResCual=psnr(imread('Config4Resultado.bmp'),imread('ResultadoEsperado.bmp'))

errPSNRResCual2=psnr(imread('Config4Resultado.bmp'),imread('Muestras/Vasiliy_Kandinskiy_11.jpg'))

% Estructural similaridad

% Paciente 1

errPaciente1=ssim(imgCifrada1, imgOriginal1)
errPaciente1=ssim(imgCifrada1, imgOriginal1)

% Config 2
errSSIMConfig2=ssim(modelo1,modelo2)
errSSIMConfig3=ssim(modelo1,modelo3)
% Config 4

errSSIMConfig4=ssim(modelo1, modelo4)
errSSIMConfig5=ssim(modelo1, modelo5)

errSSIMCual2=ssim(imread('Config4Resultado.bmp'),imread('Muestras/Vasiliy_Kandinskiy_11.jpg'))