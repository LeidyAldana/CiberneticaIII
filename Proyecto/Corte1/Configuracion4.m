% Reglas implementadas en el articulo -- Configuracion 1
%  Proyecto esteganografía

%close all
%clear all
warning('off')

% Sistema
a=newfis('Config4');

% Variable de entrada: diferencia de rojo dR+dB+dG
a=addvar(a,'input','dRdBdG',[0 765]);  % Rango en la escala de colores

%Funciones de pertenencia
a=addmf(a,'input',1,'ZE','trimf',  [-318.6 0 318.6] );
a=addmf(a,'input',1,'MD','trimf',  [63.75 382.5 701.4] );
a=addmf(a,'input',1,'LR','trimf', [446.1 765 1084] );
plotmf(a,'input',1)

% Variable de salida: Similitud %
a=addvar(a,'output','Similitud',[0 100]);

a=addmf(a,'output',1,'NS','trimf', [-41.67 0 41.67] );
a=addmf(a,'output',1,'MS','trimf', [8.333 50 91.67] );
a=addmf(a,'output',1,'ES','trimf', [58.33 100 141.7] );
plotmf(a,'output',1)%Funciones de pertenencia


%Reglas de inferencia


ruleList=[
    1 3 1 1
    2 2 1 1
    3 1 1 1
];

a = addRule(a,ruleList);

% Sistema difuso
fuzzy(a)

% Evaluar el sistema [dR, dG, dB]
Y = evalfis([0],a)

