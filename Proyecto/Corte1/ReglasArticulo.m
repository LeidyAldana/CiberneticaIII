% Reglas implementadas en el articulo -- Configuracion 1
%  Proyecto esteganografía

%close all
%clear all
warning('off')

% Sistema
a=newfis('ModeloFuzzyArticulo');

% Variable de entrada: diferencia de rojo dR
a=addvar(a,'input','dR',[0 255]);  % Rango en la escala de colores

%Funciones de pertenencia
a=addmf(a,'input',1,'ZE','trimf', [-106.2 0 106.2]);
a=addmf(a,'input',1,'MD','trimf', [21.25 127.5 233.8]);
a=addmf(a,'input',1,'LR','trimf', [148.7 255 361.2]);
plotmf(a,'input',1)

%Variable de entrada: diferencia de Verde dG
a=addvar(a,'input','dG',[0 255]);

%Funciones de pertenencia
a=addmf(a,'input',2,'ZE','trimf', [-106.2 0 106.2]);
a=addmf(a,'input',2,'MD','trimf', [21.25 127.5 233.8]);
a=addmf(a,'input',2,'LR','trimf', [148.7 255 361.2]);
plotmf(a,'input',2)


% Variable de entrada: diferencia de azul dB
a=addvar(a,'input','dB',[0 255]);

%Funciones de pertenencia
a=addmf(a,'input',3,'ZE','trimf', [-106.2 0 106.2]);
a=addmf(a,'input',3,'MD','trimf', [21.25 127.5 233.8]);
a=addmf(a,'input',3,'LR','trimf', [148.7 255 361.2]);
plotmf(a,'input',3)


% Variable de salida: Similitud
a=addvar(a,'output','Similitud',[0 100]);

a=addmf(a,'output',1,'NS','trimf', [-41.7 0 25.61]);
a=addmf(a,'output',1,'SS','trimf', [0.578 25.1 50.18]);
a=addmf(a,'output',1,'MS','trimf', [25.4 49.91 75.9]);
a=addmf(a,'output',1,'QS','trimf', [49.8 75.07 100]);
a=addmf(a,'output',1,'ES','trimf', [75.6 99.69 127]);
plotmf(a,'output',1)%Funciones de pertenencia


%Reglas de inferencia


ruleList=[
  	1 1 1 5 1 1
   	1 1 2 5 1 1
    1 1 3 4 1 1
    1 2 1 5 1 1
    1 2 2 4 1 1
    1 2 3 3 1 1
    1 3 1 4 1 1
    1 3 2 3 1 1
    1 3 3 2 1 1
    2 1 1 5 1 1
    2 1 2 4 1 1
    2 1 3 3 1 1
    2 2 1 4 1 1
    2 2 2 3 1 1
    2 2 3 2 1 1
    2 3 1 3 1 1
    2 3 2 2 1 1
    2 3 3 1 1 1
    3 1 1 4 1 1
    3 1 2 3 1 1
    3 1 3 2 1 1
    3 2 1 3 1 1
    3 2 2 2 1 1
    3 2 3 1 1 1
    3 3 1 2 1 1
    3 3 2 1 1 1
    3 3 3 1 1 1];

a = addRule(a,ruleList);

% Sistema difuso
fuzzy(a)

% Evaluar el sistema [dR, dG, dB]
Y = evalfis([0 20 210],ModeloFuzzyArticulo)
Y = evalfis(ModeloFuzzyArticulo,[0 20 210])
