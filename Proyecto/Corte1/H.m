reference = [2.7284;
             6.0000;
             3.2407;
             4.8275;
             1.6666;
             1.8571;
             2.6000;
             6.7073;
             2.6960;
             4.0377;
             3.3064];

inputV1 = [8 2 2.48;
          10 4 2.94;
          17 3 2.4;
          12 3 2.48;
          20 3 2.18;
           5 2 3.42;
          31 3 3.6;
          31 3 6.58;
           1 3 3.23;
          12 4 2.45;
          12 4 1.29];
    
inputV2 = [96.72 2 6;
          95 4 6;
         108 3 5;
         145 3 6;
         165 3 8;
         105 2 8;
         100 3 8;
          82 3 12;
         204 3 15;
         106 4 5;
         186 4 5];
     
inputV3 = [3 96.72 0.88;
          10 95 2.75;
          17 108 1.22;
          12 145 1.785;
          20 165 0.74;
           5 105 0.74;
          31 100 1.777;
          31 182 0.79;
           1 204 0.5;
          12 106 1.77;
          12 186 2];
    
inputV4 = [96.72 2 5.5;
          95 4 8.5;
         108 3 4.5;
         145 3 10;
         165 3 3;
         105 2 3;
         100 3 3.1;
         182 3 2.5;
         204 3 3;
         106 4 5;
         186 4 9];

ModelV1 = evalfis(pruebaConceptoInmuebles, inputV1);
ModelV2 = evalfis(pruebaConceptoInmueblesV2, inputV2);
ModelV3 = evalfis(pruebaConceptoInmueblesV3, inputV3);
ModelV4 = evalfis(pruebaConceptoInmueblesV4, inputV4);

%Presentacion de los resultados Modelo #1
figure
hold on
plot(reference,'b')
plot(ModelV1,'r')
hold off
title('Salida Modelo 1','Fontsize',16,'FontName','Times New Roman')
xlabel('n','Fontsize',16,'FontName','Times New Roman')
ylabel('y(n)','Fontsize',16,'FontName','Times New Roman')
set(gca,'FontSize',16,'LineWidth',1.0,'FontName','Times New Roman')
legend('Real','Simulado')
box
 
%Calculo del error
errorM1 = ModelV1-reference;
 
%Figura del error
figure
plot(errorM1)
title('Error M1','Fontsize',16,'FontName','Times New Roman')
xlabel('n','Fontsize',16,'FontName','Times New Roman')
ylabel('e(n)','Fontsize',16,'FontName','Times New Roman')
set(gca,'FontSize',16,'LineWidth',1.0,'FontName','Times New Roman')
 
%Calculo el indice de desempeño MSE (Error Cuadratico Medio)
MSE_Modelo1 = 1/length(errorM1)*sum(errorM1.^2)


%Presentacion de los resultados Modelo #2
figure
hold on
plot(reference,'b')
plot(ModelV2,'r')
hold off
title('Salida Modelo 2','Fontsize',16,'FontName','Times New Roman')
xlabel('n','Fontsize',16,'FontName','Times New Roman')
ylabel('y(n)','Fontsize',16,'FontName','Times New Roman')
set(gca,'FontSize',16,'LineWidth',1.0,'FontName','Times New Roman')
legend('Real','Simulado')
box
 
%Calculo del error
errorM2 = ModelV2-reference;
 
%Figura del error
figure
plot(errorM2)
title('Error M2','Fontsize',16,'FontName','Times New Roman')
xlabel('n','Fontsize',16,'FontName','Times New Roman')
ylabel('e(n)','Fontsize',16,'FontName','Times New Roman')
set(gca,'FontSize',16,'LineWidth',1.0,'FontName','Times New Roman')
 
%Calculo el indice de desempeño MSE (Error Cuadratico Medio)
MSE_Modelo2 = 1/length(errorM2)*sum(errorM2.^2)

%Presentacion de los resultados Modelo #3
figure
hold on
plot(reference,'b')
plot(ModelV3,'r')
hold off
title('Salida Modelo 3','Fontsize',16,'FontName','Times New Roman')
xlabel('n','Fontsize',16,'FontName','Times New Roman')
ylabel('y(n)','Fontsize',16,'FontName','Times New Roman')
set(gca,'FontSize',16,'LineWidth',1.0,'FontName','Times New Roman')
legend('Real','Simulado')
box
 
%Calculo del error
errorM3 = ModelV3-reference;
 
%Figura del error
figure
plot(errorM3)
title('Error M3','Fontsize',16,'FontName','Times New Roman')
xlabel('n','Fontsize',16,'FontName','Times New Roman')
ylabel('e(n)','Fontsize',16,'FontName','Times New Roman')
set(gca,'FontSize',16,'LineWidth',1.0,'FontName','Times New Roman')
 
%Calculo el indice de desempeño MSE (Error Cuadratico Medio)
MSE_Modelo3 = 1/length(errorM3)*sum(errorM3.^2)


%Presentacion de los resultados Modelo #4
figure
hold on
plot(reference,'b')
plot(ModelV4,'r')
hold off
title('Salida Modelo 4','Fontsize',16,'FontName','Times New Roman')
xlabel('n','Fontsize',16,'FontName','Times New Roman')
ylabel('y(n)','Fontsize',16,'FontName','Times New Roman')
set(gca,'FontSize',16,'LineWidth',1.0,'FontName','Times New Roman')
legend('Real','Simulado')
box
 
%Calculo del error
errorM4 = ModelV4-reference;
 
%Figura del error
figure
plot(errorM4)
title('Error M4','Fontsize',16,'FontName','Times New Roman')
xlabel('n','Fontsize',16,'FontName','Times New Roman')
ylabel('e(n)','Fontsize',16,'FontName','Times New Roman')
set(gca,'FontSize',16,'LineWidth',1.0,'FontName','Times New Roman')
 
%Calculo el indice de desempeño MSE (Error Cuadratico Medio)
MSE_Modelo4 = 1/length(errorM4)*sum(errorM4.^2)