% Sistema
a=newfis('Config3');

% Variable de entrada: diferencia de rojo dR+dB+dG
a=addvar(a,'input','dRdBdG',[0 765]);  % Rango en la escala de colores

%Funciones de pertenencia
a=addmf(a,'input',1,'ZE','gaussmf',  [135.3 -8.882e-15]);
a=addmf(a,'input',1,'MD','gaussmf',  [135.4 382.5]);
a=addmf(a,'input',1,'LR','gaussmf', [135.5 765] );
plotmf(a,'input',1)

% Variable de entrada: diferencia de rojo dR+dB+dG
a=addvar(a,'input','posicion',[0 192024]);  % Rango en la escala de colores

%Funciones de pertenencia
a=addmf(a,'input',2,'ZE','gaussmf', [3.396e+04 -2.229e-12]);
a=addmf(a,'input',2,'MD','gaussmf', [3.399e+04 9.601e+04]);
a=addmf(a,'input',2,'LR','gaussmf', [3.401e+04 1.92e+05]);
plotmf(a,'input',1)

% Variable de salida: Similitud %
a=addvar(a,'output','Similitud',[0 100]);

a=addmf(a,'output',1,'NS','gaussmf', [17.7 -1.332e-15]);
a=addmf(a,'output',1,'MS','gaussmf',  [17.7 50]);
a=addmf(a,'output',1,'ES','gaussmf', [17.71 100]);
plotmf(a,'output',1)%Funciones de pertenencia


%Reglas de inferencia


ruleList=[
    1 1 3 1 1
    1 2 3 1 1
    1 3 3 1 1
    2 1 2 1 1
    2 2 2 1 1
    2 3 2 1 1
    3 1 1 1 1
    3 2 1 1 1
    3 3 1 1 1
];

a = addRule(a,ruleList);

% Sistema difuso
fuzzy(a)

% Evaluar el sistema [dR, dG, dB]
Y = evalfis([0],a)
