% Taller 1  Punto 4

function f = ControlD(e)
% Controlador difuso basado en el error

% Error positivo A (epa)

%e = (0:0.1:7)';
erPosA = smf(e, [1.8 2.3]);
%plot(e, erPosA);

% Error positivo B (epb)

%e = (0:0.1:7)';
erPosB = dsigmf(e, [0.309 -20 -0.1839 36.6]);
%plot(e, erPosB);

% Error negativo B (enb)

%e = (0:0.1:7)';
erNegB = smf(e, [0.9 1.4]);
%plot(e, erNegB);

% Error negativo A (ena)

erNegA = dsigmf(e, [0.309 -20 0.219 36.6]);

% Ecuaciones

Y1=erNegA;
Y2=erNegB;
Y3=0;
Y4=erPosB;
Y5=erPosA;

% Actuadores

ung= 1.0;
unp= 0.442;
uz = 0;
upp = -0.211;
upg = -0.97;

%Salida total

f = ung*Y1 + unp*Y2 + uz*Y3 + upp*Y4 + upg*Y5;
