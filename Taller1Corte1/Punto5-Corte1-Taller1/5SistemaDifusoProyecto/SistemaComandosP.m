% Ejercicio del sistema de l�gica difusa para esteganografia
% implementado en comandos
% Punto 5 Corte 1 ~ 20151020019 ~ Proyecto

close all
clear all
warning('off')

% Sistema
a=newfis('Esteganografia');

% Variable de entrada: diferencia de rojo dR
a=addvar(a,'input','dR',[0 255]);  % Rango en la escala de colores

%Funciones de pertenencia
a=addmf(a,'input',1,'cero','gaussmf',[49.99 7.2]);
a=addmf(a,'input',1,'grande','gaussmf',[53.93 255]);
plotmf(a,'input',1)

%Variable de entrada: diferencia de Verde dG
a=addvar(a,'input','dG',[0 255]);

%Funciones de pertenencia
a=addmf(a,'input',2,'cero','trapmf',[-72.2 -0.324 63 130.8]);
a=addmf(a,'input',2,'grande','trapmf',[132.2 209.6 259.2 336.6]);
plotmf(a,'input',2)


% Variable de entrada: diferencia de azul dB
a=addvar(a,'input','dB',[0 255]);

%Funciones de pertenencia
a=addmf(a,'input',3,'cero','gaussmf',[49.99 7.2]);
a=addmf(a,'input',3,'medio','gaussmf',[38.02 138]);
a=addmf(a,'input',3,'grande','gaussmf',[53.93 255]);
plotmf(a,'input',3)


% Variable de salida: Similitud
a=addvar(a,'output','Similitud',[0 255]);

a=addmf(a,'output',1,'ES','trimf', [-106 0 62.9]);
a=addmf(a,'output',1,'MS','trimf',[39.3 76.3 109.2]);
a=addmf(a,'output',1,'MDS','trimf',[94.4 133 170.9]);
a=addmf(a,'output',1,'PS','trimf',[148.3 188 217]);
a=addmf(a,'output',1,'NS','trimf',[198.7 255 361]);
plotmf(a,'output',1)%Funciones de pertenencia


%Reglas de inferencia


ruleList=[
  	1 1 1 1 1 1
   	1 1 2 1 1 1
    2 1 2 1 1 1
   	2 2 2 1 1 1
    2 2 3 1 1 1];

a = addRule(a,ruleList);

% Sistema difuso
fuzzy(a)

% Evaluar el sistema [temperatura, tiempo, vapor]
Y = evalfis([0 20 210],a)
