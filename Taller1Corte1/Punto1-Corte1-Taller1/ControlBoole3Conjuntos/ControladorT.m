% Taller 1  Punto 1
function Ft = ControladorT(X)
%Funci�n que implementa el controlador Booleano 

%Sensores
D= X(1);
C= X(2);
B= X(3);
A= X(4);
%Funciones de activaci�n

F1 = max((1-D),min((1-A),C));
F2 = (1-B);
F3 = (1-C);

%Actuadores

q1 = 0.03;
q2 = 0.034;
q3 = 0.036;

%Flujo Total

Ft = F1*q1 + F2*q2 + F3*q3;	