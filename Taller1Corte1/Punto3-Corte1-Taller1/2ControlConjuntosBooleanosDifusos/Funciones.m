%Funciones de pertenencia difusa


%Funciones trapezoidales
h = (0:0.1:7)';
B = trapmf(h, [0.5 1.5 100 100]);
M = trapmf(h, [2.5 3.5 100 100]);
A = trapmf(h, [4.5 5.5 100 100]);


%Funciones sigmoidales
x = 0:0.1:10;
subplot(311); plot(x, smf(x, [2 8]));
subplot(312); plot(x, smf(x, [4 6]));
subplot(313); plot(x, smf(x, [6 4]));

figure
plot(h,B,h,M,h,A);

