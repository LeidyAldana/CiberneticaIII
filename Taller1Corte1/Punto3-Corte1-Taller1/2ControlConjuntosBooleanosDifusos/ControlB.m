% Taller 1  Punto 3 
function ft = ControlB(h)
%Dise�o del controlador con conjuntos Booleanos
%Entrada: h
%Salida: ftdir

%Sensor Bajo
if h < 0.2
    D = 0;
else
    D = 1;
end

%Sensor Medio Bajo
if h < 0.4
    C = 0;
else
    C = 1;
end

%Sensor Medio Alto
if h < 0.8
    B = 0;
else
    B = 1;
end
        
%Sensor Muy Alto
if h < 1.0
    A = 0;
else
    A = 1;
end

%Funciones de activacion

F1 = max((1-D),min((1-A),C));
F2 = (1-B);
F3 = (1-C);
%Actuadores

%Actuadores

q1 = 0.10;
q2 = 0.105;
q3 = 0.110;

%Flujo Total

ft = F1*q1 + F2*q2 + F3*q3;