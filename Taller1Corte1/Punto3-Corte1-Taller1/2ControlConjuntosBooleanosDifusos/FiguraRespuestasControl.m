%Comparación de los controladores

clear all
close all

%Datos de las variables
h = (0:0.1:7)';
n = length(h);

%Evaluación controlador Booleano 
for i=1:n
    Fb(i) = ControlB(h(i));
end

%Evaluación controlador conjuntos difusos
for i=1:n
    Fd(i) = ControlD(h(i));
end
%Figura de la comparación
hold on
title('Comparación acciones de control')
xlabel('h [m]')
ylabel('f [Lt/s]')
legend('Booleanos','Difusos')
plot(h,Fb,h,Fd)
hold off
axis([0 2 0 0.7])


