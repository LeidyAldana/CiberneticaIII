% Taller 1  Punto 3 

function ft = ControlD(h)
%Dise�o del controlador con conjuntos difusos

%Funcion que implementa el controlador booleano
%Entrada: h
%Salida: ftdir

%Sensor Bajo

%h = (0:0.1:7)';
%D = trapmf(h,[0.2 0.4 100 100]);
D = trapmf(h,[0.1 0.2 100 100]);
%plot(h, D);

%Sensor Medio
%h = (0:0.1:7)';
%C = smf(h,[0.4 0.8]);
C = smf(h,[0.2 0.4]);
%C = trapmf(h,[0.21 0.4 100 100]);
%plot(h, C);

%Sensor Alto

%h = (0:0.1:7)';
%B = smf(h,[0.8 1.1]);
%B = trapmf(h,[0.8 1.1 100 100]);
B = trapmf(h,[0.4 0.85 100 100]);
%plot(h, B);

%Sensor Muy Alto

%h = (0:0.1:7)';
%A = trapmf(h,[1.1 1.3 100 100]);
A = trapmf(h,[0.85 1.1 100 100]);
%plot(h, A);

%Funciones de activacion

F1 = max((1-D),min((1-A),C));
F2 = (1-B);
F3 = (1-C);




%Actuadores

q1 = 0.18;
q2 = 0.205;
q3 = 0.229;

q1 = 0.199;
q2 = 0.215;
q3 = 0.255;


q1 = 0.0319*4;
q2 = 0.0320*6;
q3 = 0.0321*7;

q1 = 0.182;
q2 = 0.185;
q3 = 0.189;

%Flujo Total

ft = F1*q1 + F2*q2 + F3*q3;