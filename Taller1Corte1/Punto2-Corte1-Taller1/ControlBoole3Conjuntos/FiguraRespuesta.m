% Relacion de entrada salida

%Valores de y

y = 0:0.01:1.2;

% Longitud de y

N = length(y);

%Evaluar todos los datos 

for i=1:N
    f(i) = ControladorT(y(i));
end

%Figura de respuesta

plot(y,f)

xlabel('y [m]')
ylabel('F ')
title('Entrada / salida')